package no9变态跳台阶;

/**
 * 一只青蛙一次可以跳上1级台阶，也可以跳上2级……它也可以跳上n级。求该青蛙跳上一个n级的台阶总共有多少种跳法。
 *
 * @author Xiaoyu
 * @date 2020/7/22 - 21:08
 */
public class Solution {
    public int JumpFloorII(int target) {
        /**
         * 看题目是贪心
         * 那我就假设他最好一次就能直接跳上去,最差是一次一次得跳
         * 也就是他只要在一节,我就可以认为他能从之前的任何一级跳上来
         */
        int[] step = new int[target + 1];
        step[0] = 1;//初始化
        step[1] = 1;
        for (int i = 2; i <= target; i++) {
            step[i] = 2 * step[i - 1];
        }
        return step[target];
    }
}
