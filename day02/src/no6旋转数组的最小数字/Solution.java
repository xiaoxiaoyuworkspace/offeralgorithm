package no6旋转数组的最小数字;

import java.util.Arrays;

/**
 * @author Xiaoyu
 * @date 2020/7/22 - 19:53
 */
public class Solution {
    /**
     * 思路
     * 1.整个旋转后的数组肯定是部分有序的
     * 2.先去有序的里面找
     * 3.如果不在有序区间，就递归去找无序区间
     */
    private int min = 0;

    public int minNumberInRotateArray(int[] array) {
        //非空校验
        if (array == null || array.length == 0) return 0;
        int len = array.length;
        min = array[len / 2];//初始化最小值
        minNum(Arrays.copyOfRange(array, 0, len / 2));
        minNum(Arrays.copyOfRange(array, len / 2 + 1, len));
        return min;
    }

    private void minNum(int[] arr) {
        //如果数组为空,就不比较了
        if(arr == null || arr.length==0) return;
        int length = arr.length;
        //如果这个是有序的并且最小值比他小
        if(arr[length-1]>arr[0]||length==1) {
            min = Math.min(min,arr[0]);
        }else {
            //剩下无序,继续找最小值
            min=Math.min(min,arr[length/2]);
            minNum(Arrays.copyOfRange(arr, 0, length / 2));
            minNum(Arrays.copyOfRange(arr, length / 2 + 1, length));
        }

    }
}
