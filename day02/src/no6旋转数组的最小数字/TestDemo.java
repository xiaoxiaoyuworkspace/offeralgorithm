package no6旋转数组的最小数字;

import org.junit.Test;

/**
 * @author Xiaoyu
 * @date 2020/7/22 - 20:15
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s= new Solution();
        int[] arr = {1,1,1,0,1};
        int i = s.minNumberInRotateArray(arr);
        System.out.println(i);
    }
}
