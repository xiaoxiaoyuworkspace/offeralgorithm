package no8跳台阶;

/**一只青蛙一次可以跳上1级台阶，也可以跳上2级。求该青蛙跳上一个n级的台阶总共有多少种跳法（先后次序不同算不同的结果）。
 * @author Xiaoyu
 * @date 2020/7/22 - 20:51
 */
public class Solution {
    public int JumpFloor(int target) {
        /*
            动态规划
            1.1或者2级,总共n级
         */
        if(target==0) return 0;
        int dp[] = new int[target+1];
        dp[0]=1;
        //初始化,1,2级都是一次调到
        dp[1]=1;
        for (int i = 2; i <= target; i++) {
            dp[i] = dp[i-1]+dp[i-2];//每次都有能从i层的下1或者2跳上来
        }
        return dp[target];
    }
}
