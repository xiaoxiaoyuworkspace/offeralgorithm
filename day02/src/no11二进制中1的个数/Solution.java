package no11二进制中1的个数;

/**
 * @author Xiaoyu
 * @date 2020/7/22 - 22:39
 */
public class Solution {
    public int NumberOf1(int n) {
        int res = 0;
        //每次将最右边的1变成0
        while(n != 0) {
            //从右边往左边统计1的个数
            res++;
            //n-1后,n最右边的1的右边的0全部变成1,在经过与运算后,将包括最右边的1的右边所有变成0
            n &= n - 1;
        }
        return res;
    }
}
