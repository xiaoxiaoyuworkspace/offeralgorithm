package no10矩形覆盖;

/**
 * @author Xiaoyu
 * @date 2020/7/22 - 21:31
 */
public class Solution {
    public int RectCover(int target) {
        /**
         * 求个数的总共,且可以运用上次的累加一般是动态规划
         * 1.n=1方法为1
         * 2.n=2方法切割两个n=1的
         * 3.n=3方法是切割成三个n=1和一个n=1和一个n=2
         * 4.n=5也是n=1,n=3和n=2,n=2
         * 5.n=5也是n=1,n=4和n=2,n=3.....
         */
        if(target<=2) return target;
        int[] dp = new int[target+1];
        dp[0] = 0;
        dp[1] =1;
        dp[2] = 2;
        for (int i = 3; i <= target; i++) {
            dp[i] = dp[i-1]+dp[i-2];
        }
        return dp[target];
    }
}
