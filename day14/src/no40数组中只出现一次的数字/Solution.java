package no40数组中只出现一次的数字;

import java.util.Arrays;

/**
 * 一个整型数组里除了两个数字之外，其他的数字都出现了两次。
 * 请写程序找出这两个只出现一次的数字。
 *
 * @author Xiaoyu
 * @date 2020/8/3 - 21:38
 */
public class Solution {
    //num1,num2分别为长度为1的数组。传出参数
    //将num1[0],num2[0]设置为返回结果
    public void FindNumsAppearOnce(int[] array, int num1[], int num2[]) {
        int sum=0;
        //得到异或结果，即为不相同两个数的异或结果sum
        for(int num:array)
            sum^=num;
        //得到sum的二进制的1的最低位
        int flag=(-sum)&sum;
        int res[]=new int[2];
        //分成两个组进行异或，每组异或后的结果就是不相同两个数的其中之一
        for(int num:array){
            if((flag&num)==0)
                res[0]^=num;
            else{
                res[1]^=num;
            }
        }
        num1[0]=res[0];
        num2[0]=res[1];

    }
}
