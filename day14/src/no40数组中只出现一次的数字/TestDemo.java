package no40数组中只出现一次的数字;

import org.junit.Test;

/**
 * @author Xiaoyu
 * @date 2020/8/3 - 22:07
 */
public class TestDemo {
    @Test
    public void test() {
        int[] arr = {1,2,3,3,1,5,2,5,8,100};
        int res = 0;
        for (int i : arr) {
            res ^=i;
        }
        int i = -(res) & res;
        System.out.println(i);
        System.out.println(res);
    }
}
