package no41和为S的连续正数序列;

import java.util.ArrayList;

/**
 * @author Xiaoyu
 * @date 2020/8/3 - 23:04
 */
public class Solution {
    public ArrayList<ArrayList<Integer>> FindContinuousSequence(int sum) {
        /**
         * 注意是连续的序列,设第一个数为i,最后一个数为j
         * sum=(i+j)*项数/2
         * 项数=j-i+1
         * 所以sum=(i+j)*(j-i+1)/2
         * 所以sum*2=(i+j)*(j-i+1)
         * 前项可以是1~sum/2
         * sum*2+i^2-i=j(j+1)
         * (int) Math.floor(Math.sqrt(左边))=j
         */
        ArrayList<ArrayList<Integer>> res = new ArrayList<> ();
        if(sum==0) return res;//非空校验
        for (int i = 1; i <= sum/2; i++) {
            int cal = sum*2+i*i-i;//获得sum*2+i^2-i=j(j+1)等号左边的值
            int last = (int) Math.floor(Math.sqrt(cal));//因为是正数,所以直接开方取下限获取j
            //看看是不是满足sum
            if((i+last)*(last-i+1)/2==sum) {
                //将i~last加入集合中再加入res
                ArrayList<Integer> list = new ArrayList<> ();
                for (int j = i; j <= last; j++) {
                    list.add(j);
                }
                res.add(list);
            }
        }
        return res;
    }
}
