package no41和为S的连续正数序列;

import org.junit.Test;

import java.util.ArrayList;

/**
 * @author Xiaoyu
 * @date 2020/8/3 - 23:22
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s = new Solution();
        ArrayList<ArrayList<Integer>> res = s.FindContinuousSequence(100);
        System.out.println(res);
    }
}
