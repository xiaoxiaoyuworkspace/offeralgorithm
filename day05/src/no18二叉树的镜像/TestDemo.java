package no18二叉树的镜像;

import org.junit.Test;

/**
 * @author Xiaoyu
 * @date 2020/7/25 - 21:41
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s = new Solution();
        TreeNode root = new TreeNode(1);
        root.left = new TreeNode(2);
        root.right = new TreeNode(3);
        root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        s.Mirror(root);
    }
}
