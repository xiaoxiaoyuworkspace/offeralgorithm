package no18二叉树的镜像;


/**
 * @author Xiaoyu
 * @date 2020/7/25 - 21:33
 */
public class Solution {
    public void Mirror(TreeNode root) {
        //非空校验
        if(root == null) return ;
        if(root.left==null&&root.right==null) return;
        //交换结点的左右子树
        TreeNode temp = root.left;
        root.left = root.right;
        root.right = temp;
        if(root.left!=null) Mirror(root.left);//递归交换左子树的左右子树
        if(root.right!=null) Mirror(root.right);//递归交换右子树的左右子树
    }
}
class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;

    }

}