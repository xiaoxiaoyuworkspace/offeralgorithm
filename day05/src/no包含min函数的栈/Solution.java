package no包含min函数的栈;

import java.util.Stack;

/**
 * @author Xiaoyu
 * @date 2020/7/25 - 23:01
 */
public class Solution {
    /**
     * 这里可以使用空间换时间
     * 每次压栈两个,一个是当前值,一个是当前最小值
     * 取最小值只要取最新压进去的值就可以了
     */
    Stack<Integer> stack = new Stack<>();

    public void push(int node) {
        //如果当前栈为空,直接将当前值和最小值压入
        if (stack.isEmpty()) {
            stack.push(node);
            stack.push(node);
        }else {
            //否则,比较最小值
            Integer min = stack.peek();
            if(node<min) {
                //如果比最小值小,将当前值压入栈
                stack.push(node);
                stack.push(node);
            }else {
                stack.push(node);
                stack.push(min);
            }
        }
    }

    public void pop() {
        stack.pop();
        stack.pop();
    }

    public int top() {
        return stack.get(stack.size()-2);
    }

    public int min() {
        //考虑使用空间换时间
        return stack.peek();
    }
}
