package no19顺时针打印矩阵;

import org.junit.Test;

import java.util.ArrayList;

/**
 * @author Xiaoyu
 * @date 2020/7/25 - 22:37
 */
public class TestDemo {
    @Test
    public void test() {
        int[][] matrix = {
                {1/*, 2, 3, 4*/},
                {5/*, 6, 7, 8*/},
                {9/*, 10, 11, 12*/},
                {13/*, 14, 15, 16*/},
        };
        Solution s= new Solution();
        ArrayList<Integer> res = s.printMatrix(matrix);
        System.out.println(res);
    }
}
