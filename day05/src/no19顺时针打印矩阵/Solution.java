package no19顺时针打印矩阵;

import java.util.ArrayList;

/**
 * @author Xiaoyu
 * @date 2020/7/25 - 22:08
 */
public class Solution {
    public ArrayList<Integer> printMatrix(int [][] matrix) {
        /**
         * 经典顺时针打印矩阵，设置边界顺时针打
         */
        ArrayList<Integer> res = new ArrayList<>();
        if(matrix==null||matrix.length==0) return res;
        int row = matrix.length;//行的数量
        int col = matrix[0].length;//列的数量
        int start = 0;
        int colLim = col-1;
        int rowLim = row-1;
        while(start<=colLim&&start<=rowLim) {
            int rowCurr = start;
            int colCurr = start;
            //上排横向右
            while(colCurr<=colLim) {
                res.add(matrix[rowCurr][colCurr++]);
            }
            //colCurr移动到了col位置，要-1
            colCurr--;
            //右竖排向下移动
            while(rowCurr<rowLim){
                res.add(matrix[++rowCurr][colCurr]);
            }
            //rowCurr移动到了row位置，要-1
//            rowCurr--;
            //下横排左移动
            while(start<rowLim&&colCurr>start) {
                res.add(matrix[rowCurr][--colCurr]);
            }
            //到了matrix[start][rowLimit]位置
            while(start<colLim&&rowCurr>start+1) {
                res.add(matrix[--rowCurr][colCurr]);
            }
            start++;
            rowLim--;
            colLim--;
        }
        return res;
    }
}
