package no46孩子们的游戏;

/**孩子们的游戏(圆圈中最后剩下的数)
 * 每年六一儿童节,牛客都会准备一些小礼物去看望孤儿院的小朋友,今年亦是如此。
 * HF作为牛客的资深元老,自然也准备了一些小游戏。
 * 其中,有个游戏是这样的:首先,让小朋友们围成一个大圈。
 * 然后,他随机指定一个数m,让编号为0的小朋友开始报数。
 * 每次喊到m-1的那个小朋友要出列唱首歌,然后可以在礼品箱中任意的挑选礼物,并且不再回到圈中,
 * 从他的下一个小朋友开始,继续0...m-1报数....这样下去....直到剩下最后一个小朋友,可以不用表演,
 * 并且拿到牛客名贵的“名侦探柯南”典藏版(名额有限哦!!^_^)。请你试着想下,哪个小朋友会得到这份礼品呢？
 * (注：小朋友的编号是从0到n-1)
 *
 * 如果没有小朋友，请返回-1
 * @author Xiaoyu
 * @date 2020/8/5 - 23:35
 */
public class Solution {
    public int LastRemaining_Solution(int n, int m) {
        /**
         * 约瑟夫环问题
         */
        if(n==0) return -1;
        Node head = new Node(0);//第一个小朋友做头
        Node curr = head;//遍历指针
        for (int i = 1; i < n; i++) {
            curr.next=new Node(i);//当前结点的下一个结点指向新结点
            curr=curr.next;
        }
        curr.next = head;//形成环
        Node pre = curr;//curr的前置结点
        curr =curr.next;//遍历指针回到头结点
        while(n>1) {
            //向后移动m-1次
            for (int i = 0; i < m - 1; i++) {
                pre=pre.next;
                curr=curr.next;
            }
            //curr移动到了m-1的位置,移除这个结点
            pre.next=curr.next;
            curr.next=null;
            curr=pre.next;
            n--;
        }
        return curr.val;
    }
}
/**
 * 构建结点
 */
class Node {
    int val;
    Node next;
    public Node(int val) {
        this.val = val;
    }
}
