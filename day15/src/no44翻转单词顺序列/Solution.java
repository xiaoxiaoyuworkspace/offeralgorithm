package no44翻转单词顺序列;

/**牛客最近来了一个新员工Fish，每天早晨总是会拿着一本英文杂志，写些句子在本子上。
 * 同事Cat对Fish写的内容颇感兴趣，有一天他向Fish借来翻看，但却读不懂它的意思。
 * 例如，“student. a am I”。
 * 后来才意识到，这家伙原来把句子单词的顺序翻转了，正确的句子应该是“I am a student.”。
 * Cat对一一的翻转这些单词顺序可不在行，你能帮助他么？
 * @author Xiaoyu
 * @date 2020/8/5 - 21:45
 */
public class Solution {
    public String ReverseSentence(String str) {
        /**
         * 用空格来分割字符串,然后将得到的字符数组逆序返回就可以了
         */
        if(str== null || str.length() ==0) return str;
        //如果字符串全是空格,就直接返回字符串
        int check = 0;
        while(check<str.length()&&str.charAt(check)==' ') {
            check++;
        }
        if(check==str.length()) return str;
        //开始分割字符,根据是" "
        String[] strs = str.split(" ");
        StringBuilder sb = new StringBuilder();
        for (int i = strs.length - 1; i >= 0; i--) {
            sb.append(strs[i]);
            sb.append(" ");//注意要把空格补回来
        }
        return sb.deleteCharAt(sb.length()-1).toString();//移除最后额外添加的空格
    }

    public static void main(String[] args) {
        Solution s =new Solution();
        String str=" ";
        String res = s.ReverseSentence(str);
        System.out.println(res);
    }
}
