package no45扑克牌顺子;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * LL今天心情特别好,因为他去买了一副扑克牌,发现里面居然有2个大王,2个小王(一副牌原本是54张^_^)...
 * 他随机从中抽出了5张牌,想测测自己的手气,看看能不能抽到顺子,如果抽到的话,他决定去买体育彩票,嘿嘿！！
 * “红心A,黑桃3,小王,大王,方片5”,“Oh My God!”不是顺子.....
 * LL不高兴了,他想了想,决定大\小 王可以看成任何数字,并且A看作1,J为11,Q为12,K为13。
 * 上面的5张牌就可以变成“1,2,3,4,5”(大小王分别看作2和4),“So Lucky!”。LL决定去买体育彩票啦。
 * 现在,要求你使用这幅牌模拟上面的过程,然后告诉我们LL的运气如何，
 * 如果牌能组成顺子就输出true，否则就输出false。为了方便起见,你可以认为大小王是0。
 *
 * @author Xiaoyu
 * @date 2020/8/5 - 22:14
 */
public class Solution {
    public boolean isContinuous(int[] numbers) {
        /**
         * 直接遍历数组,每次将相邻两个牌的点数相减,最后得到的点数如果小于定于大小鬼数,就可以变形成功
         */
        if(numbers == null || numbers.length ==0) return false;
        int ghosts =0;//记录大小王数量
        int last=0;//上一个数
        int counts = 0;//统计相差点数
        Arrays.sort(numbers);
        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i]==0) {
                ghosts++;//统计大小鬼数量
            }else {
                //如果第一次找到非大小王的数,就用它当基准值
                if(last ==0) {
                    last =numbers[i];//
                }else{
                    if(last == numbers[i]) return false;//有重复的牌
                    counts+=(numbers[i]-last);//统计点数
                    counts--;
                    last = numbers[i];//让现在这个数成为上一个数
                }

            }
        }
        return Math.abs(counts)<=ghosts;//看看点数绝对值是不是比大小王数量少,如果少,就可以用大小王来变成顺子

    }
    public boolean isContinuous1(int[] numbers) {
            if(numbers==null||numbers.length==0) return false;
            //排序+set,如果set最大值和最小值<5就可以
            Set<Integer> set = new HashSet<>();
            int min = 14;
            int max = 0;
            for(int num : numbers) {
                if(set.contains(num)) return false;
                if(num!=0) {
                    set.add(num);
                    min = Math.min(min,num);
                    max = Math.max(max,num);
                }
            }
            return max-min<5;
        }
}
