package no43左旋转字符串;

/**汇编语言中有一种移位指令叫做循环左移（ROL），现在有个简单的任务，就是用字符串模拟这个指令的运算结果。
 * 对于一个给定的字符序列S，请你把其循环左移K位后的序列输出。
 * 例如，字符序列S=”abcXYZdef”,要求输出循环左移3位后的结果，即“XYZdefabc”。是不是很简单？OK，搞定它！
 * @author Xiaoyu
 * @date 2020/8/5 - 21:19
 */
public class Solution {
    public String LeftRotateString(String str,int n) {
        /**
         * 向左移动n个位置，就是将字符串前(n%字符串长度)拼接在字符串后面
         */
        if(str==null||str.length()==0||n%str.length()==0) return str;
        n%=str.length();
        StringBuilder sb = new StringBuilder();
        for (int i = n; i < str.length(); i++) sb.append(str.charAt(i));//将后面的字符串拼到前面
        for (int j = 0; j < n; j++) sb.append(str.charAt(j));//将要拼接的前面放在后面
        return sb.toString();
    }
}
