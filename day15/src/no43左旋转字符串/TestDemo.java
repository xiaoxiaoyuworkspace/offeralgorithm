package no43左旋转字符串;

import org.junit.Test;

/**
 * @author Xiaoyu
 * @date 2020/8/5 - 21:29
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s =new Solution();
        String str = s.LeftRotateString("abcXYZdef", 10);
        System.out.println(str);
    }
}
