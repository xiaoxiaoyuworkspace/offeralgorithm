package no42和为S的两个数字;

import java.util.ArrayList;

/**
 * @author Xiaoyu
 * @date 2020/8/5 - 20:35
 */
public class Solution {
    public ArrayList<Integer> FindNumbersWithSum(int [] array, int sum) {
        ArrayList<Integer> res = new ArrayList<>();
        if(array==null || array.length ==0||array[array.length-1]*2<=sum||array[0]*2>=sum) return res;
        /**1,3,7,8,10,13,17    16
         * a+b=sum.如果sum不变，a增加，b需要减少
         * 定义两个指针，头尾指针
         * 左边定住，右边移动，如果找到a+b=sum,则加入res,头指针后移，尾指针前移；
         * 如果a+b<sum了，说明移动尾指针找不到了，直接开始下一次循环；
         * 比如1,2,3,4,5,6
         */
        int last =array.length-1;
        int min = Integer.MAX_VALUE;
        int ret1 = 0;
        int ret2 = 0;
        for (int first = 0; first < array.length; first++) {
            if(first>=last) break;
            while (first<last) {
                ret1 = array[first];
                ret2 = array[last];
                if(ret1+ret2==sum&&ret1*ret2<min) {
                    res.add(array[first]);
                    res.add(array[last]);
                    min = ret1 * ret2;
                    last--;
                }else if(ret1+ret2<sum){
                    break;
                }else {
                    last--;
                }
            }
        }
        return res;

    }
}
