package no5两个栈实现队列;

import java.util.Stack;

/**
 * @author Xiaoyu
 * @date 2020/7/21 - 22:26
 */
public class Solution {
    /**
     * 1.队列先进先出,栈先进后出
     * 2.使用两个栈模拟先进先出
     */
    Stack<Integer> stack1 = new Stack<Integer>();
    Stack<Integer> stack2 = new Stack<Integer>();

    public void push(int node) {
        stack1.push(node);//入队
    }

    public int pop() {
        //出队是先进先出,栈是先进后出,所以留下最后一个
        //如果栈2位空,就将栈1元素放到栈2,从先->后变成逆序
        if(stack2.isEmpty()) {
            while (!stack1.isEmpty()) {
                stack2.push(stack1.pop());
            }
        }
        return stack2.pop();
    }
}
