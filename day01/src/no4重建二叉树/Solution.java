package no4重建二叉树;


import java.util.Arrays;

/**
 * 输入某二叉树的前序遍历和中序遍历的结果，请重建出该二叉树。
 * 假设输入的前序遍历和中序遍历的结果中都不含重复的数字。
 * 例如输入前序遍历序列{1,2,4,7,3,5,6,8}和中序遍历序列{4,7,2,1,5,3,8,6}，则重建二叉树并返回。
 *
 * @author Xiaoyu
 * @date 2020/7/21 - 21:20
 */
public class Solution {
    public TreeNode reConstructBinaryTree(int[] pre, int[] in) {
        /*
        * 1.首先前序遍历能找到根节点
        * 2.根据中序遍历能找到属于左子树和右子树的结点
        * 3.递归过程为找根节点,找左子树,右子树,然后对左子树右子树找根节点,找左子树右子树....
        * */
        int len = pre.length;//结点个数
        if(len==0) return null;
        TreeNode root = new TreeNode(pre[0]);//构造根节点
        for (int i = 0; i < len; i++) {
            //找到对应根节点
            if(pre[0]==in[i]) {
                root.left = reConstructBinaryTree(Arrays.copyOfRange(pre,1,i+1),Arrays.copyOfRange(in,0,i));//构造左子树
                root.right = reConstructBinaryTree(Arrays.copyOfRange(pre,i+1,len),Arrays.copyOfRange(in,i+1,len));//构造右子树
                break;
            }
        }
        return root;
    }
}

class TreeNode {
    int val;
    TreeNode left;
    TreeNode right;

    TreeNode(int x) {
        val = x;
    }
}
