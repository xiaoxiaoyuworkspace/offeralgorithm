package no2替换空格;

/**请实现一个函数，将一个字符串中的每个空格替换成“%20”。
 * 例如，当字符串为We Are Happy.则经过替换之后的字符串为We%20Are%20Happy。
 * @author Xiaoyu
 * @date 2020/7/21 - 20:45
 */
public class Solution {
    public String replaceSpace(StringBuffer str) {
        //非空校验
        if(str==null||str.length()==0) return "";
        int len = str.length();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < len; i++) {
            char c = str.charAt(i);
            //如果当前字符为空，则替换
            if(c==' ') {
                sb.append("%20");
            }else sb.append(c);
        }
        return sb.toString();
    }
}
