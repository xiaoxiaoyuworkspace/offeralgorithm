package no1二维数组中的查找;

import org.junit.Test;

/**
 * @author Xiaoyu
 * @date 2020/7/21 - 11:22
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s = new Solution();
        int[][] arr= {
                {1,2,8,9},
                {2,4,9,12},
                {4,7,10,13},
                {6,8,11,15}};
        boolean find = s.Find(7, arr);
        System.out.println(find);
    }
}
