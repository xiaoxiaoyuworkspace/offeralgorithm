package no3从尾到头打印链表;

import java.util.ArrayList;

/**
 * @author Xiaoyu
 * @date 2020/7/21 - 21:07
 */
public class Solution {
    ArrayList<Integer> res = new ArrayList<>();

    public ArrayList<Integer> printListFromTailToHead(ListNode listNode) {
        if(listNode==null) return res;
        printNode(res, listNode);
        return res;
    }

    private void printNode(ArrayList<Integer> res, ListNode listNode) {
        if (listNode == null) return;
        printNode(res,listNode.next);
        res.add(listNode.val);
    }
}

class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}

