package no49把字符串转换成整数;

/**将一个字符串转换成一个整数，要求不能使用字符串转换整数的库函数。 数值为0或者字符串不是一个合法的数值则返回0
 输入描述:
 输入一个字符串,包括数字字母符号,可以为空
 输出描述:
 如果是合法的数值表达则返回该数字，否则返回0
 * @author Xiaoyu
 * @date 2020/8/6 - 22:39
 */
public class Solution {
    public int StrToInt(String str) {
        if(str== null || str.length() ==0) return 0;
        char[] chars = str.toCharArray();
        if(!(chars[0]=='+'||chars[0]=='-'||(chars[0]>='0'&&chars[0]<='9'))) return 0;
        int cal =chars[0]=='-'?-1: 1;//如果是+或者数，那就做1乘，否则就是-1乘
        long sum =0;
        int curr = (chars[0]=='+'||chars[0]=='-')?1:0;//定义遍历起点,如果第一位是+或者-,就移动到第二位
        if(curr>=chars.length||chars[curr]=='0') return 0;//第一位越界或者是0,直接返回错误
        while(curr<chars.length) {
            if(!(chars[curr]>='0'&&chars[curr]<='9')) return 0;
            long i = chars[curr]-'0';
            sum=i+(sum*10);
            if(sum>2147483647) return 0;//如果超出int返回,返回错误
            curr++;
        }
        return (int)sum*cal;
    }

    public static void main(String[] args) {
        Solution s =new Solution();
        String str="-2147 483649";
        int i = s.StrToInt(str);
        System.out.println(i);
    }
}
