package no50数组中重复的数字;

/**在一个长度为n的数组里的所有数字都在0到n-1的范围内。
 * 数组中某些数字是重复的，但不知道有几个数字是重复的。也不知道每个数字重复几次。
 * 请找出数组中任意一个重复的数字。
 * 例如，如果输入长度为7的数组{2,3,1,0,2,5,3}，那么对应的输出是第一个重复的数字2。
 * @author Xiaoyu
 * @date 2020/8/6 - 23:12
 */
public class Solution {
    // Parameters:
    //    numbers:     an array of integers
    //    length:      the length of array numbers
    //    duplication: (Output) the duplicated number in the array number,length of duplication array is 1,so using duplication[0] = ? in implementation;
    //                  Here duplication like pointor in C/C++, duplication[0] equal *duplication in C/C++
    //    这里要特别注意~返回任意重复的一个，赋值duplication[0]
    // Return value:       true if the input is valid, and there are some duplications in the array number
    //                     otherwise false
    public boolean duplicate(int numbers[],int length,int [] duplication) {
        if(numbers==null||numbers.length ==0) return false;//非空校验
        int[] hash = new int[length];//使用表来记录出现次数
        int res = -1;//记录第一个出现的重复数
        for (int i = 0; i < numbers.length; i++) {
            if(hash[numbers[i]]==0)hash[numbers[i]]++;//如果对应数字的索引的值为0,说明第一次出现,将它对应次数+1
            else {
                //如果不是第一次出现,那就记录该重复数字,跳出循环
                res=numbers[i];
                break;
            }
        }
        duplication[0]=res==-1?duplication[0]:res;
        return res!=-1;

    }
}
