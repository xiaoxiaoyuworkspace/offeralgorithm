package no47求1加到n;

/**求1+2+3+...+n
 * 求1+2+3+...+n，要求不能使用乘除法、for、while、if、else、switch、case等关键字及条件判断语句（A?B:C）。
 * @author Xiaoyu
 * @date 2020/8/6 - 21:49
 */
public class Solution {
    public int Sum_Solution(int n) {
        int sum =n;
        boolean b = n>0 && ((sum+=Sum_Solution(n-1))>0);//短路现象，只要&&前面逻辑是false，直接返回false，后面不执行
        return sum;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        int i = s.Sum_Solution(5);
        System.out.println(i);
    }
}
