package no48不用加减乘除做加法;

/**写一个函数，求两个整数之和，要求在函数体内不得使用+、-、*、/四则运算符号。

 * @author Xiaoyu
 * @date 2020/8/6 - 22:17
 */
public class Solution {
    public int Add(int num1,int num2) {
        /**
         * 使用进位与非进位的和
         * num1+num2可以用他们的进位+非进位的和表示
         * 进位=num1^num2
         * 非进位=(num1&num2)<<1
         * 当进位和为0时，返回非进位，就是最终值
         */
        //假设num1为进位，num2为非进位
        if(num2==0) return num1;
        int in=num1^num2;//进位
        int noIn = (num1&num2)<<1;
        return Add(in,noIn);
    }
}
