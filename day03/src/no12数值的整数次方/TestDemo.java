package no12数值的整数次方;

import org.junit.Test;

/**
 * @author Xiaoyu
 * @date 2020/7/23 - 22:11
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s= new Solution();
        double power = s.Power(2.0, 10);
        System.out.println(power);
    }
}
