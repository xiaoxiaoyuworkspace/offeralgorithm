package no12数值的整数次方;

/**
 * @author Xiaoyu
 * @date 2020/7/23 - 22:05
 */

public class Solution {
    public double Power(double base, int exponent) {
        //使用二分快速幂
        if(base==0) return 0;
        //考虑到指数大小，用Long替换int
        long e = exponent;
        //如果指数是负数，那要先变成整数
        if(e<0) {
            base = 1/base;
            e = -e;
        }
        //开始运算x^n = x^n/2 *x^n/2 = ((x^2^))^n/2
        //要把指数降成0，由于有可能是奇数，奇数时会多出一项x，直接乘进结果就行
        double res =1.0;//用于和x相乘获取最终值
        while(e>0) {
            //如果是奇数
            if(e%2==1) {
                res*=base;//将多出的x直接乘入res，最后一次肯定是1，直接将所有base底数乘进去
            }
            e/=2;
            base*=base;//每次二分都会将新的base^2作为底数
        }
        return res;
    }
}
