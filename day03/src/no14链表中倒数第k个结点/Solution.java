package no14链表中倒数第k个结点;

/**
 * @author Xiaoyu
 * @date 2020/7/23 - 23:31
 */
public class Solution {
    public ListNode FindKthToTail(ListNode head, int k) {
        /**
         * 很明显的双指针，两个之间相差k个位置，后面一个到尾部，前面一个就是倒数第k个
         */
        if (head == null) return null;
        ListNode fir = head;//定义第一个指针
        ListNode sec = head;//定义第二个指针
        //找到它在的位置
        while (k > 0) {
            sec = sec.next;
            k--;
            if (sec == null && k > 0) return null;
        }
        while (sec != null) {
            fir = fir.next;
            sec = sec.next;
        }
        return fir;
    }
}

class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}