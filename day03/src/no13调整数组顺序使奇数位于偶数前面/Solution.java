package no13调整数组顺序使奇数位于偶数前面;

import java.util.ArrayList;
import java.util.List;

/**输入一个整数数组，实现一个函数来调整该数组中数字的顺序，使得所有的奇数位于数组的前半部分，
 * 所有的偶数位于数组的后半部分，并保证奇数和奇数，偶数和偶数之间的相对位置不变
 * @author Xiaoyu
 * @date 2020/7/23 - 22:18
 */
public class Solution {
    public void reOrderArray1(int [] array) {
        /**
         * 1.数组顺序未知，需要排序后相对未知不变
         * 2.如果独立一个空间只存放偶数值，然后将对应个数拼接在后面
         * 3.设置一个指针来遍历，另一个保存奇数索引
         */
        //非空校验
        if(array==null||array.length==0) return;
        //初始化两个指针
        int oddCurr = 0;
        int evenCurr = 0;
        //找到偶数
        while(evenCurr<array.length) {

            evenCurr++;
        }

    }
    public void reOrderArray(int [] array) {
        /**
         * 1.数组顺序未知，需要排序后相对未知不
         * 2.位置值左端点，右端点，双向指针解决问题
         * 3.但是要保持相对未知不变，最好还是借用外部数组
         */
        //非空校验
        if(array==null||array.length==0) return;
        List<Integer> list1 = new ArrayList<>();
        List<Integer> list2 = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            //如果是奇数，加前面，如果是偶数加后面
            if(array[i]%2==1) list1.add(array[i]);
            else list2.add(array[i]);
        }
        int[] res = new int[array.length];
        int size = list1.size();
        for (int j1 = 0; j1 < size; j1++) {
            res[j1] = list1.get(j1);
        }
        for (int j2 = 0; j2 < list2.size(); j2++) {
            res[size+j2] = list2.get(j2);
        }
        for (int j = 0; j < array.length; j++) {
            array[j]=res[j];
        }
        for (int j : array) {
            System.out.println(j);
        }
    }
}
