package no13调整数组顺序使奇数位于偶数前面;

import org.junit.Test;

/**
 * @author Xiaoyu
 * @date 2020/7/23 - 22:11
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s= new Solution();
        int[] arr={1,2,3,4,5,6,7};
        s.reOrderArray(arr);
    }
}
