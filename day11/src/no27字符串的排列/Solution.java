package no27字符串的排列;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * 输入一个字符串,按字典序打印出该字符串中字符的所有排列。
 * 例如输入字符串abc,则按字典序打印出由字符a,b,c所能排列出来的所有字符串abc,acb,bac,bca,cab和cba。
 * 输入一个字符串,长度不超过9(可能有字符重复),字符只包括大小写字母。
 *
 * @author Xiaoyu
 * @date 2020/7/31 - 20:43
 */
public class Solution {
    public ArrayList<String> Permutation(String str) {
        ArrayList<String> strs = new ArrayList<>();
        //非空校验
        if (str == null || str.length() == 0) return strs;
        char[] chars = str.toCharArray();//将字符串咋换成数组
        Arrays.sort(chars);//排序
        boolean[] isUsed = new boolean[str.length()];//看看哪些用过
        StringBuilder sb = new StringBuilder();//存放使用过的字符
        //dfs遍历
        dfs(strs, sb, chars, isUsed);
        return strs;
    }

    private void dfs(ArrayList<String> strs, StringBuilder sb, char[] chars, boolean[] isUsed) {
        //如果添加完毕
        if (sb.length() == chars.length) {
            strs.add(sb.toString());
            return;
        }
        //定义一个之指针记录上次使用的字母
        char lastChar = ' ';
        for (int i = 0; i < chars.length; i++) {
            //如果该字母没有被使用过,加入当前位置
            if (lastChar!=chars[i]&&!isUsed[i]) {
                sb.append(chars[i]);//如果该字母没有被使用过,加入当前位置
                isUsed[i] = true;
                dfs(strs, sb, chars, isUsed);
                lastChar = chars[i];
                sb.deleteCharAt(sb.length() - 1);//剪枝
                isUsed[i] = false;
            }
        }

    }
}
