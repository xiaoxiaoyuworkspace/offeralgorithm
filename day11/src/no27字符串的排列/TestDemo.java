package no27字符串的排列;

import org.junit.Test;

import java.util.ArrayList;

/**
 * @author Xiaoyu
 * @date 2020/7/31 - 21:16
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s= new Solution();
        ArrayList<String> res = s.Permutation("aab");
        System.out.println(res);
    }
}
