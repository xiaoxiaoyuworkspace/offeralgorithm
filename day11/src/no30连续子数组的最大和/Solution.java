package no30连续子数组的最大和;

/**HZ偶尔会拿些专业问题来忽悠那些非计算机专业的同学。
 * 今天测试组开完会后,他又发话了:在古老的一维模式识别中,常常需要计算连续子向量的最大和,当向量全为正数的时候,问题很好解决。
 * 但是,如果向量中包含负数,是否应该包含某个负数,并期望旁边的正数会弥补它呢？
 * 例如:{6,-3,-2,7,-15,1,2,2},连续子向量的最大和为8(从第0个开始,到第3个为止)。
 * 给一个数组，返回它的最大连续子序列的和，你会不会被他忽悠住？(子向量的长度至少是1)
 * @author Xiaoyu
 * @date 2020/7/31 - 23:37
 */
public class Solution {
    public int FindGreatestSumOfSubArray(int[] array) {
        /**
         *         设f(i)为i位置的子序列的最大和
         *         那么f(i) = max(nums[i],f(i-1)+nums[i])
         *         为什么呢？考虑的状态转移就是这个数加入上一个数的子序列，或者自己单独成子序列
         */
        //非空校验
        if(array==null||array.length==0) return 0;
        int[] dp = new int[array.length+1];
        dp[1] = array[0];//初始化
        int res = array[0];
        //状态转移方程是
        for (int i = 2; i <= array.length; i++) {
            if(dp[i-1]>0) dp[i] = dp[i-1]+array[i-1];//如果上一个子序列大于0才会有正作用，就可以吧当前数合并到上一个子序列
            else dp[i] = array[i-1];//上一个子序列没有正作用，直接用他本身当序列
            res = Math.max(res,dp[i]);//保留最大值
        }
        return res;
    }
}
