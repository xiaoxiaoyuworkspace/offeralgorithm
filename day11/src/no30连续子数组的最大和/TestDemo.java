package no30连续子数组的最大和;

import org.junit.Test;

/**
 * @author Xiaoyu
 * @date 2020/8/1 - 0:01
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s = new Solution();
        int[] arr = {6,-3,-2,7,-15,1,2,2};
        int i = s.FindGreatestSumOfSubArray(arr);
        System.out.println(i);
    }
}
