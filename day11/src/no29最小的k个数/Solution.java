package no29最小的k个数;

import java.util.ArrayList;

/**输入n个整数，找出其中最小的K个数。例如输入4,5,1,6,2,7,3,8这8个数字，则最小的4个数字是1,2,3,4。
 * @author Xiaoyu
 * @date 2020/7/31 - 22:55
 */
public class Solution {
    public ArrayList<Integer> GetLeastNumbers_Solution(int [] input, int k) {
        ArrayList<Integer> res = new ArrayList<> ();
        if(input==null||input.length==0||k==0) return res;
        int length = input.length;//定义长度
        if(k>length) return res;
        //将k个数的集合先填满
        for (int j = 0; j < k; j++) {
            res.add(input[j]);
        }
        res.sort((o1, o2) -> o1 - o2);//排序
        for (int m = k; m < length; m++) {
            //看看能否插入集合
            if(input[m]<res.get(k-1)) {
                //找到集合插入位置
                for (int i = 0; i < res.size(); i++) {
                    if(input[m]<=res.get(i)) {
                        res.add(i,input[m]);
                        break;
                    }
                }
                res.remove(res.size()-1);//移出集合中多余的那个最大数
            }
        }
        return res;
    }
}
