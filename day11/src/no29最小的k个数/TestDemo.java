package no29最小的k个数;

import org.junit.Test;

import java.util.ArrayList;

/**
 * @author Xiaoyu
 * @date 2020/7/31 - 23:18
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s =new Solution();
        int[] arr = {4,5,1,6,2,7,3,8};
        ArrayList<Integer> list = s.GetLeastNumbers_Solution(arr, 10);
        System.out.println(list);
    }
}
