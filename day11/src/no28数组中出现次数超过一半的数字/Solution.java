package no28数组中出现次数超过一半的数字;

/**
 * @author Xiaoyu
 * @date 2020/7/31 - 22:18
 */
public class Solution {
    public int MoreThanHalfNum_Solution(int[] array) {
        /**
         * 使用博弈法/摩尔投票法
         */
        if (array == null || array.length == 0) return 0;
        int times = 1;//记录重复数字出现次数
        int king = array[0];//记录当前出现最多的数字
        int len = array.length;//总长度
        for (int i = 1; i < array.length; i++) {
            //如果博弈完了,直接重置博弈
            if (times == 0) {
                times = 1;
                king = array[i];
                continue;
            }
            if (array[i] == king) times++;
            else times--;
        }
        //这道题需要考虑没有多数存在情况,那么验证
        int count = 0;
        for (int j = 0; j < array.length; j++) {
            if (king == array[j]) count++;
        }
        return count > len / 2 ? king : 0;
    }
}
