package no24二叉树中和为某一值的路径;

import java.util.ArrayList;

/**
 * @author Xiaoyu
 * @date 2020/7/27 - 21:11
 */
public class Solution {
    ArrayList<ArrayList<Integer>> res = new ArrayList<>();

    public ArrayList<ArrayList<Integer>> FindPath(TreeNode root, int target) {
        if (root == null) return res;
        ArrayList<Integer> list = new ArrayList<>();
        dfsFind(list, root, target);
        return res;
    }

    private void dfsFind(ArrayList<Integer> list, TreeNode root, int target) {
        list.add(root.val);//加入当前值
        //如果是叶子节点
        if(root.left==null&&root.right==null) {
            if (target-root.val == 0) {
                res.add(new ArrayList<>(list));//将List拷贝一份加入res，这样回溯时候不会影响最终结果
            }
            return;
        }
        //如果左子树不为空,递归左子树
        if(root.left!=null) {
            dfsFind(list, root.left, target - root.val);
            list.remove(list.size() - 1);
        }
        //如果右子树不为空,递归右子树
        if(root.right!=null) {
            dfsFind(list, root.right, target - root.val);
            list.remove(list.size() - 1);
        }

    }
}

class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;

    }

}