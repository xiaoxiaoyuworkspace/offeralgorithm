package no26二叉搜索树与双向链表;


/**
 * @author Xiaoyu
 * @date 2020/7/27 - 22:59
 */
public class Solution {
    TreeNode pre = null;//设置前驱
    TreeNode head = null;//设置头结点

    public TreeNode Convert(TreeNode pRootOfTree) {
        /**
         * 1.中序遍历二叉搜索树
         * 2.给每个节点连接前后关系,头结点前驱是尾巴,尾节点后继是头
         */
        //非空校验
        if (pRootOfTree == null) return null;
        dfs(pRootOfTree);//对二叉搜索树进行中序遍历
        //中序遍历完以后,pre指针会指向链表末尾,也就是最大的数,也就是树的最右下的结点
        pre.right = head;//尾节点后继指向头结点
        head.left = pre;//右节点前驱指向尾节点
        return head;
    }
    //左  根   右进行中序遍历
    private void dfs(TreeNode root) {
        if (root == null) return;
        dfs(root.left);//先遍历左左子树
        if (pre == null) {
            head = root;//如果前驱没有指向,说明还在头结点遍历
            pre = root;//如果前驱没有指向,说明还在头结点遍历
        }
        else {
            pre.right = root;//否则,当前结点是前驱结点的后继
            root.left = pre;//因为是双向链表,当前结点的前驱要有指向
            pre = root;//处理完当前结点,就将前驱结点后移
        }
        dfs(root.right);//处理右子树
    }
}

class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;

    }

}