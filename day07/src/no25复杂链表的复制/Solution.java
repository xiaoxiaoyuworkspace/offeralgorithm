package no25复杂链表的复制;

/**
 * @author Xiaoyu
 * @date 2020/7/27 - 22:01
 */
public class Solution {
    public RandomListNode Clone(RandomListNode pHead) {
        /**
         * 暴力法
         * 首先将链表clone
         * 然后在找random
         * 找到每个random下标,逐一去指
         */
        if (pHead == null) return null;
        RandomListNode newNode = new RandomListNode(0);//虚拟头结点
        RandomListNode curr = newNode;//新链表遍历指针
        RandomListNode oldCurr = pHead;//设置旧链表遍历
        //复制next的链表
        while (oldCurr != null) {
            curr.next = new RandomListNode(oldCurr.label);//连向新创建结点
            curr = curr.next;
            oldCurr = oldCurr.next;
        }
        RandomListNode newCurr = newNode.next;//设置新链表遍历指针
        RandomListNode copy = pHead;//设置copy的指针
        while (copy!=null) {
            RandomListNode currPHead = pHead;
            RandomListNode currNewNode = newNode.next;//新节点的遍历结点

            //复制random的指向
            while (currPHead != copy.random) {
                //找到pHead的random的位置
                currPHead = currPHead.next;
                currNewNode = currNewNode.next;
            }
            //找到了后赋值
            newCurr.random = currNewNode;
            newCurr=newCurr.next;
            copy = copy.next;
        }
        return newNode.next;
    }
}

class RandomListNode {
    int label;
    RandomListNode next = null;
    RandomListNode random = null;

    RandomListNode(int label) {
        this.label = label;
    }
}