package no25复杂链表的复制;

import org.junit.Test;

/**
 * @author Xiaoyu
 * @date 2020/7/27 - 22:38
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s= new Solution();
        RandomListNode head = new RandomListNode(0);
        head.next = new RandomListNode(3);
        RandomListNode node = new RandomListNode(5);
        head.next.next = node;
        head.next.random = node;
        RandomListNode clone = s.Clone(head);
        System.out.println(clone);
    }
}
