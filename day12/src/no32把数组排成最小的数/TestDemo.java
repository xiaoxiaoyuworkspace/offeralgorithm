package no32把数组排成最小的数;

import org.junit.Test;

/**
 * @author Xiaoyu
 * @date 2020/8/1 - 21:18
 */
public class TestDemo {
    @Test
    public void test() {
        String s1 = "1";
        String s2 = "10";
        String str1 = s1 + s2;
        String str2 = s2 + s1;
        System.out.println(str1);
        System.out.println(str2);
        System.out.println(str1.compareTo(str2));
        Solution s = new Solution();
        int[] arr = {5,32,321};
        String s3 = s.PrintMinNumber(arr);
        System.out.println(s3);

    }
}
