package no32把数组排成最小的数;

import java.util.ArrayList;
import java.util.Comparator;

/**输入一个正整数数组，把数组里所有数字拼接起来排成一个数，打印能拼接出的所有数字中最小的一个。
 * 例如输入数组{3，32，321}，则打印出这三个数字能排成的最小数字为321323。
 * @author Xiaoyu
 * @date 2020/8/1 - 20:57
 */
public class Solution {
    public String PrintMinNumber(int [] numbers) {
        /**
         * 1.要对集合中数据进行排序
         * 2.要想拿到最小值，需要使用一个传递公式(s1 + s2>s2+s1)则s1<s2;最后再将字符串拼接即可
         * 3.例如10和1，"10"+"1"="101";"1"+"10"="110".这里"101"<"110"，所以排序中"10"要大于"1"
         */
        if(numbers == null || numbers.length ==0) return "";
        ArrayList<String> list = new ArrayList<>();
        //将所有数字转换成字符串加入到集合中
        for (int i = 0; i < numbers.length; i++) {
            list.add(String.valueOf(numbers[i]));
        }
        list.sort((o1, o2) -> (o1+o2).compareTo((o2+o1)));//对字符串集合进行排序
        return String.join("",list);
    }
}
