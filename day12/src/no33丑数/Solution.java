package no33丑数;

/**把只包含质因子2、3和5的数称作丑数（Ugly Number）。
 * 例如6、8都是丑数，但14不是，因为它包含质因子7。
 * 习惯上我们把1当做是第一个丑数。求按从小到大的顺序的第N个丑数。
 * @author Xiaoyu
 * @date 2020/8/1 - 22:36
 */
public class Solution {
    public int GetUglyNumber_Solution(int index) {
        /**
         * 分析：
         * 1.先列出前面的丑数1,2,3,4,5,6,8,9...他们的共同特点就是可以通过235因数相乘得到
         * 2.可以设置三个指针2,3,5指针l2,l3,l5
         * 3.最小的数是三个因数指针数*他们自己对应的值中最小的一个
         * 4.f(i)为第i+1个最小丑数，每次找到一个丑数，就将对应的指针后移
         *
         */
        if(index<=0) return 0;//根据错误提交，默认index<=0返回0
        int[] dp = new int[index];
        dp[0] =1;//初始化dp[0]为第一个丑数
        int l2=0,l3=0,l5=0;//定义l2,l3,l5为2,3,5因子指针
        for (int i = 1; i < index; i++) {
            dp[i]=Math.min(Math.min(dp[l2]*2,dp[l3]*3),dp[l5]*5);
            //找到对应指针后移
            if(dp[i]==dp[l2]*2) l2++;
            if(dp[i]==dp[l3]*3) l3++;
            if(dp[i]==dp[l5]*5) l5++;
        }
        return dp[index-1];
    }
}
