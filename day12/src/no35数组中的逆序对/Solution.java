package no35数组中的逆序对;

/**在数组中的两个数字，如果前面一个数字大于后面的数字，则这两个数字组成一个逆序对。
 * 输入一个数组,求出这个数组中的逆序对的总数P。并将P对1000000007取模的结果输出。
 * 即输出P%1000000007
 * @author Xiaoyu
 * @date 2020/8/1 - 23:28
 */
public class Solution {
    public static  final int MOD = 1000000007;
    public int InversePairs(int [] array) {
        if(array==null||array.length ==0) return 0;
        /**
         * 暴力法
         */
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < i; j++) {
                if(array[j]>array[i]) count++;
            }
        }
        return count%MOD;
    }
}
