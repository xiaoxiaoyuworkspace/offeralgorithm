package no34第一个只出现一次的字符;

import java.util.ArrayList;

/**在一个字符串(0<=字符串长度<=10000，全部由字母组成)中找到第一个只出现一次的字符,并返回它的位置,
 * 如果没有则返回 -1（需要区分大小写）.（从0开始计数）
 * @author Xiaoyu
 * @date 2020/8/1 - 22:57
 */
public class Solution {
    public int FirstNotRepeatingChar(String str) {
        if(str == null || str.length() ==0) return -1;
        //找两个hash表进行记录字母出现次数
        int[] hash1 = new int[26];//记录小写字母
        int[] hash2 = new int[26];//记录大写字母
        ArrayList<Integer> list = new ArrayList<> ();//记录每个字符首次出现的索引
        char[] chars = str.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            //如果是小写字母
            if(chars[i]>='a'&&chars[i]<='z') {
                int index = chars[i] - 'a';
                //如果首次记录
                if(hash1[index]==0) {
                    list.add(i);//将字符索引添加
                }
                hash1[index]++;//记录次数
            }else{
                //如果是大写字母
                int index = chars[i] - 'A';
                //如果首次记录
                if(hash2[index]==0) {
                    list.add(i);//将字符索引添加
                }
                hash2[index]++;//记录次数
            }
        }
        //遍历集合，看看谁是第一个只出现一次的
        for (Integer in : list) {
            //如果对应的是小写字母并且对应只出现一次，那就是首次出现的
            if((chars[in]>='a'&&chars[in]<='z')&&hash1[chars[in]-'a']==1) return in;
            if((chars[in]>='A'&&chars[in]<='Z')&&hash2[chars[in]-'A']==1) return in;
        }
        return -1;
    }
}
