package no34第一个只出现一次的字符;

import org.junit.Test;

/**
 * @author Xiaoyu
 * @date 2020/8/1 - 23:16
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s = new Solution();
        String str = "aaaabccd";
        int i = s.FirstNotRepeatingChar(str);
        System.out.println(i);
    }
}
