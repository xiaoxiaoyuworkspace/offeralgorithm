package no62二叉搜索樹的第k个结点;


import org.junit.Test;

import java.util.ArrayList;

/**
 * @author Xiaoyu
 * @date 2020/7/25 - 21:41
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s = new Solution();

        TreeNode root = new TreeNode(6);
       root.left = new TreeNode(2);
        root.right = new TreeNode(10);
         root.left.left = new TreeNode(1);
        root.left.right = new TreeNode(3);
        root.right.left = new TreeNode(8);
        root.right.right = new TreeNode(11);
        TreeNode treeNode = s.KthNode(root, 4);
        System.out.println(treeNode.val);
    }
}
