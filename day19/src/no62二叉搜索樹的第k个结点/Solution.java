package no62二叉搜索樹的第k个结点;



/**
 * @author Xiaoyu
 * @date 2020/8/10 - 21:17
 */
public class Solution {
    TreeNode res ;
    int count;
    TreeNode KthNode(TreeNode pRoot, int k) {
        /**
         * 从二叉搜索树的结构上来看
         * 二叉搜索树的中序遍历就是从小到大遍历的
         * 直接统计中序遍历的顺序即可
         */
        if(k<1) return null;
        count = k-1;//计数器
        midCurr(pRoot);
        return res;
    }

    private void midCurr(TreeNode pRoot) {
        //如果到了叶子结点,就将k-1
        if(pRoot==null||count<0) return;
        if(pRoot.left!=null)         midCurr(pRoot.left);
        if(count==0) {
            res = pRoot;
            count--;
            return;
        }
        count--;
        if(pRoot.right!=null)         midCurr(pRoot.right);
    }
}

class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;

    }

}