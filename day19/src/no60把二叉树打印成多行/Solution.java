package no60把二叉树打印成多行;

import java.util.ArrayList;
import java.util.LinkedList;

/**从上到下按层打印二叉树，同一层结点从左至右输出。每一层输出一行。
 * @author Xiaoyu
 * @date 2020/8/10 - 20:58
 */
public class Solution {
    ArrayList<ArrayList<Integer>> Print(TreeNode pRoot) {
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        if(pRoot==null) return null;
        LinkedList<TreeNode> queue = new LinkedList<>();
        queue.add(pRoot);
        while (!queue.isEmpty()) {
            int size = queue.size();
            ArrayList<Integer> list = new ArrayList<>();
            while(size>0) {
                TreeNode node = queue.removeFirst();
                list.add(node.val);
                if(node.left!=null) queue.addLast(node.left);
                if(node.right!=null) queue.addLast(node.right);
                size--;
            }
            res.add(list);
        }
        return  res;
    }
}
class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;

    }

}
