package no59按之字形顺序打印二叉树;


import org.junit.Test;

import java.util.ArrayList;

/**
 * @author Xiaoyu
 * @date 2020/7/25 - 21:41
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s = new Solution();
        TreeNode root = new TreeNode(1);
       root.left = new TreeNode(2);
        root.right = new TreeNode(3);
         root.left.left = new TreeNode(4);
        root.left.right = new TreeNode(5);
        root.right.left = new TreeNode(6);
        root.right.right = new TreeNode(7);
        ArrayList<ArrayList<Integer>> print = s.Print(root);

        System.out.println(print);
    }
}
