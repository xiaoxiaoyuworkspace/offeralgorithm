package no59按之字形顺序打印二叉树;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * @author Xiaoyu
 * @date 2020/8/10 - 20:30
 */
public class Solution {
    public ArrayList<ArrayList<Integer>> Print(TreeNode pRoot) {
        /**
         * 之字形打印
         * 层序遍历就是使用队列先进后出的特性
         * 就是层序遍历的基础上,分奇偶性的不同,打印不同的顺序
         * 如果是奇数层,则从左往右打印,如果是偶数,则从右往左打印
         */
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();

        if(pRoot==null) return res;
        LinkedList<TreeNode> doubleList = new LinkedList<>();
        doubleList.add(pRoot);
        int floor = 1;//定义层数
        //如果队列为空说明已经遍历完了
        while(!doubleList.isEmpty()) {
            ArrayList<Integer> list  = new ArrayList<>();
            int size =doubleList.size();//先定义一开始进来时候的结点个数
            while (size>0) {
                TreeNode treeNode = doubleList.removeFirst();//先进先出
                //如果层数是奇数,就往后加,如果是偶数就加载首
                if(floor%2==1) {
                    list.add(treeNode.val);
                }else list.add(0,treeNode.val);
                //出一个结点填一个进去,这个是层序遍历模板
                if(treeNode.left!=null) doubleList.addLast(treeNode.left);
                if(treeNode.right!=null) doubleList.addLast(treeNode.right);
                size -- ;
            }
            res.add(list);
            floor++;
        }
        return res;
    }
}

class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;

    }

}
