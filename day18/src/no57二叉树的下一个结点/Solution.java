package no57二叉树的下一个结点;

/**
 * 给定一个二叉树和其中的一个结点，请找出中序遍历顺序的下一个结点并且返回。
 * 注意，树中的结点不仅包含左右子结点，同时包含指向父结点的指针。
 *
 * @author Xiaoyu
 * @date 2020/8/8 - 20:38
 */
public class Solution {
    public TreeLinkNode GetNext(TreeLinkNode pNode) {
        /**
         * 这道题的意思是,给你一个随机结点,包括了左右结点和父节点,相当于整棵树都给你了
         * 直接找下一个结点就好了,中序遍历是左-根-右的顺序
         * [A,B,C,D,E,F]这种就有好几种情况
         * 1.某个结点如果有右子树,根据左-根-右,直接找右子树的最左侧的结点,这个就是下一个结点
         * 2.如果这个结点没有右子树,但是他是父节点的左结点,那说明他左边已经遍历完了,该往上找了,他是左子树,所以找他的父节点
         * 3.如果这个结点没有右子树,但是他是父节点的右结点,说明他是左-根-右的最后一个,是左子树的最后一个,要找开启左-根-右逻辑的根节点,也就是找到一个结点,他是他的父节点的左节点,他的父节点就是下一个(因为遍历完他的左树了)
         * 4.根据情况三,一直找父节点找了根节点还没找到满足条件的结点,说明这个是中序遍历最后一个结点,他的下一个为空
         */
        if (pNode.right != null) {
            TreeLinkNode curr = pNode.right;
            while (curr.left != null) curr = curr.left;
            return curr;
        }
        //没有右子树
        TreeLinkNode father = pNode.next;
        if (father != null) {
            //如果该结点的父节点的左节点是该结点,那返会他的父节点
            if (father.left == pNode) return father;
            //否则往上找
            TreeLinkNode curr = father;
            //如果找到根节点还没找到,返回null;或者该结点的父节点的左结点等于该节点,则跳出循环
            while (curr.next != null && curr.next.right == curr) {
                curr = curr.next;
            }
            return curr.next;
        }
        return null;
    }
}


class TreeLinkNode {
    int val;
    TreeLinkNode left = null;
    TreeLinkNode right = null;
    TreeLinkNode next = null;

    TreeLinkNode(int val) {
        this.val = val;
    }
}