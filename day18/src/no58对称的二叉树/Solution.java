package no58对称的二叉树;


/**
 * 请实现一个函数，用来判断一棵二叉树是不是对称的。
 * 注意，如果一个二叉树同此二叉树的镜像是同样的，定义其为对称的。
 *
 * @author Xiaoyu
 * @date 2020/8/8 - 22:56
 */
public class Solution {
    boolean isSymmetrical(TreeNode pRoot) {
        if(pRoot==null) return true;
        return isSymmetrical(pRoot.left,pRoot.right);
    }
    boolean isSymmetrical(TreeNode left,TreeNode right) {
        if(left==null&&right==null) return true;//如果两个为空,直接返回true
        if(left==null||right==null) return false;//如果有一个不为空,返回false
        return left.val==right.val&&isSymmetrical(left.left,right.right)&&isSymmetrical(left.right,right.left);
    }
}

class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;

    }

}