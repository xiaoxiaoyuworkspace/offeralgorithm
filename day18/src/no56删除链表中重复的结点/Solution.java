package no56删除链表中重复的结点;

/**
 * 在一个排序的链表中，存在重复的结点，请删除该链表中重复的结点，重复的结点不保留，返回链表头指针。
 * 例如，链表1->2->3->3->4->4->5 处理后为 1->2->5
 *
 * @author Xiaoyu
 * @date 2020/8/8 - 20:07
 */
public class Solution {
    public ListNode deleteDuplication(ListNode pHead) {
        if(pHead==null||pHead.next==null) return pHead;
        ListNode dummy = new ListNode(0);//虚拟头结点
        dummy.next = pHead;
        ListNode pre = dummy;//设置一个遍历前置结点
        ListNode curr = pHead;//设置一个遍历结点
        while(curr!=null&&curr.next!=null) {
            //如果遍历到的当前节点等于下一个结点的值,那么开始找到和这个结点值相等的结点的后一个不相等的结点
            if(curr.val==curr.next.val) {
                ListNode innerCurr = curr.next;
                while(innerCurr.val==curr.val) {
                    innerCurr = innerCurr.next;
                    if(innerCurr==null) break;//如果到了末尾直接跳出循环
                }
                curr=innerCurr;//curr指向值不为选中值的后一个结点
                pre.next = innerCurr;//前置指针后继连向该结点,切断那些相等值的结点
            }else {
                //如果不相等,直接后移指针继续遍历
                pre=pre.next;
                curr = curr.next;
            }


        }
        return dummy.next;
    }
}

class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}
