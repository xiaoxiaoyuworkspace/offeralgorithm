package no37数字在排序数组中出现的次数;

import org.junit.Test;

/**
 * @author Xiaoyu
 * @date 2020/8/2 - 23:15
 */
public class TestDemo {
    @Test
    public void test() {
        Solution s = new Solution();
        int[] arr = {6,5,5,5,4,3,2,1};
        int i = s.GetNumberOfK(arr, 5);
        System.out.println(i);
    }
}
