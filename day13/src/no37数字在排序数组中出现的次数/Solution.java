package no37数字在排序数组中出现的次数;

/**
 * 统计一个数字在排序数组中出现的次数。
 *
 * @author Xiaoyu
 * @date 2020/8/2 - 22:36
 */
public class Solution {
    public int GetNumberOfK(int[] array, int k) {
        /**
         * 正向排序和逆向排序？
         * 二分查找
         */
        if (array == null || array.length == 0) return 0;
        int len = array.length;
        int start = 0;
        int end = len - 1;
        int index = -1;
        //如果是正序排序,使用正序二分
        index = binarySearch(array, start, end, k);
        if (index == -1) return 0;
        int count = 1;
        int after = index + 1;//往后找的指针
        int before = index - 1;
        //前后找值
        while (after <= end && array[after] == k) {
            count++;
            after++;
        }
        while (before >= start && array[before] == k) {
            count++;
            before--;
        }
        return count;
    }


    private int binarySearch(int[] array, int start, int end, int target) {
        int mid = start + (end - start) / 2;
        if (target == array[mid]) return mid;//找到target返回索引
        if (start >= end) return -1;//没找到
        //如果目标在左半区
        if (target > array[mid]) return binarySearch(array, start, mid, target);
        if (target < array[mid]) return binarySearch(array, mid + 1, end, target);
        return -1;
    }
}
