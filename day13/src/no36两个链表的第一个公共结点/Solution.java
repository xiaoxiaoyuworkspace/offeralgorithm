package no36两个链表的第一个公共结点;

/**输入两个链表，找出它们的第一个公共结点。
 * （注意因为传入数据是链表，所以错误测试数据的提示是用其他方式显示的，保证传入数据是正确的）
 * @author Xiaoyu
 * @date 2020/8/2 - 22:02
 */
public class Solution {
    public ListNode FindFirstCommonNode(ListNode pHead1, ListNode pHead2) {
        /*
        如果给两个指针指向两个链表，他们速度一样，如果一个到达终点，就会有一段差距
        这个差距给长那那一方
        找到多出来的部分，给长的消减掉，然后两条链表一起出发，走到一起就是焦点
         */
        ListNode curr1 = pHead1;
        ListNode curr2 = pHead2;

        while (curr1!=null&&curr2!=null) {
            curr1 = curr1.next;//向后遍历
            curr2 = curr2.next;
        }
        //如果链表1遍历到空了
        if(curr1==null) {
            //将链表2遍历到和链表1同步位置
            while(curr2!=null) {
                curr2=curr2.next;
                pHead2 =pHead2.next;
            }
        }else {
            //将链表1遍历到和链表2同步位置
            while(curr1!=null) {
                curr1=curr1.next;
                pHead1 =pHead1.next;
            }
        }
        while(pHead1!=pHead2) {
            pHead1=pHead1.next;
            pHead2=pHead2.next;
        }
        return pHead1;
    }
}
class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}