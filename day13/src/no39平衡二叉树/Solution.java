package no39平衡二叉树;

/**输入一棵二叉树，判断该二叉树是否是平衡二叉树。

 在这里，我们只需要考虑其平衡性，不需要考虑其是不是排序二叉树
 * @author Xiaoyu
 * @date 2020/8/2 - 23:47
 */
public class Solution {
    public boolean IsBalanced_Solution(TreeNode root) {
        /**
         * 首先.平衡二叉树的概念就是一个树的子树高度差不超过1
         * 要看左右边子树是不是平衡
         */
        if(root == null) return true;
        int depthDiff = depth(root.left)-depth(root.right);//左右子树的深度差
        return depthDiff>=-1&&depthDiff<=1&&IsBalanced_Solution(root.left)&&IsBalanced_Solution(root.right);//改结点的深度差小于等于1并且结点左右子树都是平衡树才返回true
    }
    //计算最大深度方法
    private int depth(TreeNode root) {
        if(root ==null) return 0;
        return Math.max(depth(root.left),depth(root.right))+1;
    }
}
class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;

    }

}