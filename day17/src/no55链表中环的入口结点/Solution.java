package no55链表中环的入口结点;

/**
 * 给一个链表，若其中包含环，请找出该链表的环的入口结点，否则，输出null。
 *
 * @author Xiaoyu
 * @date 2020/8/7 - 23:19
 */
public class Solution {
    public ListNode EntryNodeOfLoop(ListNode pHead) {
        if (pHead == null || pHead.next == null) return null;
        ListNode dummy = new ListNode(0);//虚拟头结点
        boolean hasCircle = false;//标机是否有环
        dummy.next = pHead;
        ListNode slow = dummy.next;//慢指针
        ListNode quick = pHead.next;//快指针
        //如果有环,快指针能超圈慢指针
        while (quick != null && quick.next != null) {
            slow = slow.next;
            quick = quick.next.next;
            if (quick == slow) {
                hasCircle = true;
                break;
            }

        }
        //如果有环,要慢指针从头出发,会与已经和快指针相遇的慢指针在环入口处相交
        if (hasCircle) {
            ListNode curr = dummy;
            while (curr != slow) {
                curr = curr.next;
                slow = slow.next;
            }
            return curr;
        } else return null;
    }
}

class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}
