package no52正则表达式匹配;

/**
 * 请实现一个函数用来匹配包括'.'和'*'的正则表达式。
 * 模式中的字符'.'表示任意一个字符，而'*'表示它前面的字符可以出现任意次（包含0次）。
 * 在本题中，匹配是指字符串的所有字符匹配整个模式。
 * 例如，字符串"aaa"与模式"a.a"和"ab*ac*a"匹配，但是与"aa.a"和"ab*a"均不匹配
 *
 * @author Xiaoyu
 * @date 2020/8/7 - 21:03
 */
public class Solution {
    public boolean match(char[] str, char[] pattern) {
        /**
         * 可以使用动态规划
         * f(i,j)表示str[0-i]是否与pattern[0-j]匹配
         * pattern有三种情况,p是字符/./*,分别要与str里面进行比较
         * 如果这个位置是字符且与对应位置相等,则比较f(i-1,j-1);如果是.直接比较f(i-1,j-1);如果是*则找到不是前面那个字符x,比较f(x,j-2)
         */
        boolean[][] dp = new boolean[str.length+1][pattern.length+1];
        dp[0][0] = true;
        //从第一个字符开始比较
        for (int i = 0; i <= str.length; i++) {
            for (int j = 1; j <= pattern.length; j++) {
                //如果是星号,比较f(x,j-2)
                if(pattern[j-1]=='*') {
                    //这个位置是'*'那就有多种情况了
                    //1.a*匹配0个字符,那就比较f(i,j-2)和str[i-1]的位置和pattern[j-2]的位置是不是相等(pattern[j-2]=='.')即可;2.a*匹配多个字符
                    dp[i][j] = (dp[i][j-2])||(i>0&&str[i-1]==pattern[j-2]||pattern[j-2]=='.')&&dp[i-1][j];
                }else {
                    //如果不是星号,就是字符或者是'.'
                    //这个位置f(i,j)是否匹配,要看这个位置是'.'或者str[i]等于p[j]情况下,看dp[i-1][j-1]是不是匹配
                    dp[i][j]=i>0&&(str[i-1]==pattern[j-1]||pattern[j-1]=='.')&&dp[i-1][j-1];
                }
            }
        }
        return dp[str.length][pattern.length];
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        String str = "";
        String p = ".*";
        char[] chars1 = str.toCharArray();
        char[] chars2 = p.toCharArray();
        boolean match = s.match(chars1, chars2);
        System.out.println(match);
    }
}
