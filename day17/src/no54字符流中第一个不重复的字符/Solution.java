package no54字符流中第一个不重复的字符;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

/**
 * @author Xiaoyu
 * @date 2020/8/7 - 23:02
 */
public class Solution {
    LinkedList<Character> queue = new LinkedList<>();
    Map<Character,Integer> map  = new HashMap<>();
    //Insert one char from stringstream
    public void Insert(char ch) {
        if(!map.containsKey(ch)) {
            queue.addLast(ch);
            map.put(ch,1);
        }else {
            map.put(ch,map.get(ch)+1);
        }
    }

    //return the first appearence once char in current stringstream
    public char FirstAppearingOnce() {
        while(!queue.isEmpty()) {
            Character poll = queue.removeLast();
            if(map.get(poll) == 1) return poll;
        }
        return '#';
    }
}
