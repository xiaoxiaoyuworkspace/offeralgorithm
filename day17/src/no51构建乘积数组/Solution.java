package no51构建乘积数组;

import com.sun.org.apache.bcel.internal.generic.RETURN;

/**
 * 给定一个数组A[0,1,...,n-1],请构建一个数组B[0,1,...,n-1],
 * 其中B中的元素B[i]=A[0]*A[1]*...*A[i-1]*A[i+1]*...*A[n-1]。不能使用除法。
 * （注意：规定B[0] = A[1] * A[2] * ... * A[n-1]，B[n-1] = A[0] * A[1] * ... * A[n-2];）
 * 对于A长度为1的情况，B无意义，故而无法构建，因此该情况不会存在。
 *
 * @author Xiaoyu
 * @date 2020/8/7 - 20:29
 */
public class Solution {
    public int[] multiply(int[] A) {
        /**
         * {1,2,3,4,5}应返回的就是{2*3*4*5，1*3*4*5，1*2*4*5，1*2*3*5，1*2*3*4}
         * 观察返回数组可以发现，以被去掉的数为分界点，能够拆解成左右两边计算
         * 比如n位置就等于A[0]*A[1]*...*A[n-1]     *       A[n+1]*A[n+2]*...*A[A.length-1]
         * 那么左边右边的乘积都是重复的，只要计算左右相乘就可以了
         */
        if (A == null || A.length <= 1) return new int[0];
        int[] left = new int[A.length + 1];//开一个长度为A.len+1的数组
        int[] right = new int[A.length + 1];
        left[0] = 1;//左边乘积没有数的时候.初始化为1
        right[A.length] = 1;//右边乘积没有数的时候.初始化为1
        //计算左边1-n个数的乘积
        for (int i = 1; i < left.length; i++) {
            left[i] = A[i - 1] * left[i - 1];
        }
        //计算从右往左1-n个数的乘积
        for (int i = right.length - 2; i >= 0; i--) {
            right[i] = A[i]*right[i+1];
        }
        int[] res = new int[A.length];
        //除了本身的数,左右乘积相乘
        for (int i = 0; i < res.length; i++) {
            res[i] = left[i]*right[i+1];
        }
        return res;
    }

    public static void main(String[] args) {
        Solution s =new Solution();
        int[] arr = {1,2,3,4,5};

        int[] multiply = s.multiply(arr);
        for (int i : multiply) {
            System.out.println(i);
        }
    }
}
