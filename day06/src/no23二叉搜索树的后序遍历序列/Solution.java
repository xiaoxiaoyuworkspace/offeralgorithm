package no23二叉搜索树的后序遍历序列;

import java.util.Arrays;

/**
 * @author Xiaoyu
 * @date 2020/7/26 - 22:31
 */
public class Solution {
    public boolean VerifySquenceOfBST(int [] sequence) {
        /**
         * 1.后序遍历遵循左右根的排序
         * 2.逆序查找的第一个就是当前树的根
         * 3.逆序查找，比根大的都是右子树，剩下都是左子树
         * 4.对子树递归查找
         */
        if(sequence==null||sequence.length==0) return false;
        return findSquenceOfBST(sequence);

    }

    private boolean findSquenceOfBST(int[] sequence) {
        if(sequence.length <=1) return true;
        int len = sequence.length;
        int root = sequence[len - 1];//找到根
        int point =-1;//设置左右子树分界点
        for (int i = len - 2; i >= 0; i--) {
            //找到分界点
            if(sequence[i]<root) {
                point=i;
                break;
            }
        }
        if(point == -1) return true;//表示全是右子树
        for (int j = point; j >= 0; j--) {
            if(sequence[j]>root) return false;
        }
        return findSquenceOfBST(Arrays.copyOfRange(sequence,0,point+1))&&findSquenceOfBST(Arrays.copyOfRange(sequence,point+1,len-1));

    }
}
