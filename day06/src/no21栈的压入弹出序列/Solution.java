package no21栈的压入弹出序列;

import java.util.Stack;

/**输入两个整数序列，第一个序列表示栈的压入顺序，请判断第二个序列是否可能为该栈的弹出顺序。
 * 假设压入栈的所有数字均不相等。
 * 例如序列1,2,3,4,5是某栈的压入顺序，序列4,5,3,2,1是该压栈序列对应的一个弹出序列，
 * 但4,3,5,1,2就不可能是该压栈序列的弹出序列。（注意：这两个序列的长度是相等的）
 * @author Xiaoyu
 * @date 2020/7/26 - 21:49
 */
public class Solution {
    public boolean IsPopOrder(int [] pushA,int [] popA) {
        /*
        1.如果是栈结构,将A全部压入栈
        2.A如果出栈一个,就少一个
        3.设计为入栈一个,就找出栈个数是否对应,最后应该全部出栈,否则不行
         */
        if(pushA==null||popA==null||pushA.length!=popA.length) return false;
        Stack<Integer> stack = new Stack<>();
        int popIndex = 0;//设置弹栈下标
        for (int i = 0; i < pushA.length; i++) {
            stack.push(pushA[i]);
            //如果栈不为空,并且栈顶元素等于弹栈元素,就将Stack出栈
            while(!stack.isEmpty()&&stack.peek()==popA[popIndex]){
                stack.pop();
                popIndex++;
            }
        }
        return stack.isEmpty();//如果为空则全部弹栈,说明可以
    }
}