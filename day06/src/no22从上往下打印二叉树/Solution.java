package no22从上往下打印二叉树;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;

/**从上往下打印出二叉树的每个节点，同层节点从左至右打印。
 * @author Xiaoyu
 * @date 2020/7/26 - 22:12
 */
public class Solution {
    public ArrayList<Integer> PrintFromTopToBottom(TreeNode root) {
        /**
         * 看题目描述.很明显就是层级打印
         * 层级打印使用bfs
         * bfs使用队列
         */
        ArrayList<Integer> res = new ArrayList<> ();
        //非空校验
        if(root ==null) return res;
        //经典BFS广度优先遍历
        Deque<TreeNode> queue = new ArrayDeque<>();
        queue.add(root);
        while(!queue.isEmpty()) {
            int size = queue.size();
            //将队列中原有的结点加入res,同时加入左右节点
            while(size>0) {
                TreeNode node = queue.removeFirst();//出队
                res.add(node.val);
                if(node.left!=null) queue.addLast(node.left);//如果出队的结点有左节点则将左节点入队
                if(node.right!=null) queue.addLast(node.right);//如果出队的结点有右节点则将右节点入队
                size--;
            }
        }
        return res;
    }
}
class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;

    }

}
