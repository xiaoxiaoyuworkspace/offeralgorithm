package no16合并两个排序的链表;



/**
 * @author Xiaoyu
 * @date 2020/7/24 - 21:22
 */
public class Solution {
    public ListNode Merge(ListNode list1, ListNode list2) {
        //非空校验
        if (list1 == null && list2 == null) return null;
        if (list1 == null || list2 == null) return list1 == null ? list2 : list1;
        /**
         * 归并后要完成排序
         * 1.找到数字小的一端作为合并段
         * 2.设置两个指针对两个链表遍历,如果合并段大于另一段,就将另一段的结点插入,否则继续遍历
         * 3.有一个遍历完了就将领一个接在最后
         */
        ListNode merge = null;//主合并
        ListNode mergeTo = null;//被合并
        merge = list1.val<list2.val?list1:list2;
        mergeTo = list1.val<list2.val?list2:list1;
        ListNode curr1 = merge;//主合并遍历指针
        ListNode curr2 = mergeTo;//副合并遍历指针
        ListNode preCurr1 = curr1;//curr1前置,方便插入
        curr1=curr1.next;//curr1前置,方便插入
        while(curr1!=null&&curr2!=null) {
            //如果主合并小于等于分支,那么直接后移,不用将分支合并
            if(curr1.val<=curr2.val) {
                curr1 = curr1.next;
                preCurr1 = preCurr1.next;
            }else {
                //否则要将分支插入到主合并
                mergeTo = mergeTo.next;
                preCurr1.next = curr2;
                curr2.next = curr1;//插入完成
                preCurr1=preCurr1.next;
                curr2=mergeTo;
            }
        }
        //如果主分支为空,副分支有剩余,直接拼在主分支上,否则说明副分支拼完了
        if(curr1==null) preCurr1.next=mergeTo;
        return merge;
    }
}

class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}
