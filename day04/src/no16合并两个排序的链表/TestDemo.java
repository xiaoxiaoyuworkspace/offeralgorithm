package no16合并两个排序的链表;



/**
 * @author Xiaoyu
 * @date 2020/7/24 - 22:04
 */
public class TestDemo {
    public static void main(String[] args) {
        Solution s = new Solution();
        ListNode list1 = new ListNode(1);
        list1.next = new ListNode(3);
        list1.next.next = new ListNode(5);
        list1.next.next.next = new ListNode(7);
        ListNode list2 = new ListNode(2);
        list2.next = new ListNode(4);
        ListNode merge = s.Merge(list1, list2);
        System.out.println(merge);
        System.out.println("shuchuwanbi");
    }
}
