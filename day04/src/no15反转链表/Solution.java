package no15反转链表;

import java.util.List;

/**
 * @author Xiaoyu
 * @date 2020/7/24 - 20:09
 */
public class Solution {
    public ListNode ReverseList(ListNode head) {
        //思路，将前面的结点插到后面来完成翻转
        //非空校验
        if(head==null||head.next==null) return head;
        ListNode last = head;
        ListNode curr = head;
        //找到最后一个结点
        while(last.next!=null) {
            last= last.next;
        }
        while (curr!=last) {
            curr =curr.next;
            head.next = last.next;
            last.next =head;
            head = curr;
        }
        return last;
    }

    public static void main(String[] args) {
        Solution s = new Solution();
        ListNode head = new ListNode(1);
//        head.next = new ListNode(2);
//        head.next.next = new ListNode(3);
//        head.next.next.next = new ListNode(4);
        ListNode listNode = s.ReverseList(head);
        System.out.println("shuchuwanbi");
    }
}
class ListNode {
    int val;
    ListNode next = null;

    ListNode(int val) {
        this.val = val;
    }
}
