package no17树的子结构;

/**
 * @author Xiaoyu
 * @date 2020/7/24 - 22:25
 */


class TreeNode {
    int val = 0;
    TreeNode left = null;
    TreeNode right = null;

    public TreeNode(int val) {
        this.val = val;

    }

}

public class Solution {
    public boolean HasSubtree(TreeNode root1, TreeNode root2) {
        if (root1 == null || root2 == null) return false;
        //首先两个结点的值要一致,如果一致再比较其子结构
        if (root1.val == root2.val) {
            if (isSub(root1, root2)) {
                return true;
            }
        }
        return HasSubtree(root1.left, root2) || HasSubtree(root1.right, root2);
    }

    private boolean isSub(TreeNode tree1, TreeNode tree2) {
        //如果第二个树遍历完了,那说明是tree1的子结构
        if (tree2 == null) {
            return true;
        }
        //如果母树遍历完了,那说明不是子结构
        if (tree1 == null) {
            return false;
        }
        //如果值相等则比较剩余结构
        if (tree1.val == tree2.val) return isSub(tree1.left, tree2.left)&&isSub(tree1.right, tree2.right);
        else return false;
    }
}
